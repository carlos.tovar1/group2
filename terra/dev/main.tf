module "networking" {
  source = "../modules/network"
  ec2_instance = module.compute.ec2_instance_id
}

module "compute" {
  source = "../modules/compute"

  sgp_pub = module.networking.sgp_pub_app_id
  sn_pub1 = module.networking.subnet_pub1_app_id
}