output "vpc_app_id" {
  value = aws_vpc.vpc-app.id
}

output "subnet_pub1_app_id" {
  value = aws_subnet.subnet-pub1-app.id
}

output "sgp_pub_app_id" {
  value = aws_security_group.sgp-pub-app.id
}