# VPC CREATION
resource "aws_vpc" "vpc-app" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_hostnames = true
  tags = {
    Name = "vpc-app"
  }
}

# SUBNETS CREATION
resource "aws_subnet" "subnet-pub1-app" {
  vpc_id            = aws_vpc.vpc-app.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "subnet-pub1-app"
  }
}
# INTERNET GATEWAY CREATION
resource "aws_internet_gateway" "ig-app" {
  vpc_id = aws_vpc.vpc-app.id
  tags = {
    Name = "ig-app"
  }
}

# 2 ROUTE TABLES CREATION

# PUBLIC ROUTE TABLE
resource "aws_route_table" "rt-pub-app" {
  vpc_id = aws_vpc.vpc-app.id
  route{
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.ig-app.id
  }
  tags = {
    Name = "rt-pub-app"
  }
}

# Associating the Public subnet to public route table
resource "aws_route_table_association" "rta-sn-pub1-app" {
  route_table_id = aws_route_table.rt-pub-app.id
  subnet_id = aws_subnet.subnet-pub1-app.id
}

############## SECURITY GROUPS ##################

# Defining the Public Subnet Security Group
resource "aws_security_group" "sgp-pub-app" {
  name = "sgp-pub-app"
  description = "Security Group for Public Subnet"
  vpc_id = aws_vpc.vpc-app.id
  tags = {
    Name = "sgp-pub-app"
  }
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 3000
    to_port = 3000
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}