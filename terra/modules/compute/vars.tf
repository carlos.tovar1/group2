variable "sn_pub1" {
  type = any
}

variable "sgp_pub" {
  type = any
}

variable "key_name" {
  description = "Key name for SSH into EC2"
  default = "test-gitlab"
}