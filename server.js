var http = require('http');
var path = require('path');

var express = require('express');
//  * `port` - The HTTP port to listen on. If `process.env.PORT` is set, _it overrides this value_.
var router = express();
var server = http.createServer(router);
router.use(express.static(path.resolve(__dirname, 'client')));
console.log('Booting up the server! Por favor espere hasta finalizar....')
server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
  var addr = server.address();
  console.log("Todo esta funcionando y el puerto escucha en", addr.address + ":" + addr.port);
});
